<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

// custom 404 page
//Route::get('pagenotfound', ['as' => 'notfound', 'uses' => 'HomeController@pagenotfound']);

Route::resource('events','EventsController');

Route::resource('user','UserController');

Route::get('/', 'HomeController@index');

Route::get('/calendar', 'HomeController@calendar');


Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

