<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\User;
use Auth;
use Alert;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role == 0) {
            $datas = Event::orderByDesc('start')->paginate(15);
           
        } else {
            $datas = Event::where('user_id','=', Auth::user()->id)->orderByDesc('start')->paginate(15);
        }
        
        //get count approve
        if(Auth::user()->role == 0) {
            $countApproves = Event::where('status','=','approved')->count();
        } else {
            $countApproves = Event::where('status','=','approved')->where('user_id','=', Auth::user()->id)->count();
        }
        //get count pending
        if(Auth::user()->role == 0) {
            $countPendings = Event::where('status','=','pending')->count();
        } else {
            $countPendings = Event::where('status','=','pending')->where('user_id','=', Auth::user()->id)->count();
        }

        //get count user or event if user login
        if(Auth::user()->role == 0) {
            $countUser = User::all()->count();
        } else {
         
            $countUser = Event::where('user_id','=', Auth::user()->id)->count();
        }  

        return view('home', compact('datas','countApproves','countPendings','countUser'));
    }

    public function calendar()
    {
       
        return view('calendar');
    }


    public function destroy($id)
    {
        $event  = Event::find($id);

        if($event == null) 
            return Response()->json([
                'message' => 'error delete.'
            ]); 
        
       $event->delete();

        return Response()->json([
            'message' => 'success delete.'
        ]);      
    }
}
