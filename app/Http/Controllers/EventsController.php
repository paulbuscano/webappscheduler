<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Validator;
use App\User;
use Auth;


class EventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $datas = Event::with('user')->get();
       
        return Response()->json($datas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date_start' => 'required',
            'invisible_id' => 'required',
            'thanos' => 'required',
            'title' => 'required'
        ]);

        if(Auth::user()->role == '0') {
            if($request->thanos == 'pending') {
                $color = '#ea4335';
            } else {
                $color = '#00bf4f';
            }
        } else {
            $color = '#ea4335';
        }
       
        $timestamp = strtotime($request->invisible_id) + 60*60;
      
        $time = strtotime($request->date_start);
        
        if(Auth::user()->role == '0') {
            $newformat = date('Y-m-d',$time);
        } else {
            $newformat = $request->date_end;
        }
       
        $endtime = date('H:i', $timestamp);

        $event = new Event();
        $event->title = $request->title;
        $event->start = $newformat . ' ' . $request->invisible_id;
        $event->end = $newformat . ' ' . $endtime;
        $event->color = $color;
        $event->status = $request->thanos;
        $event->user_id = Auth::user()->id;
        $event->save();
     
        return redirect('/calendar')->with('success', 'Booked Successfully');      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            '_date_start' => 'required',
            '_invisible_id' => 'required',
            '_thanos' => 'required',
            'potaena' => 'required'
        ]);  

        $timestamp = strtotime($request->_invisible_id) + 60*60;
        $endtime = date('H:i', $timestamp);

        $time = strtotime($request->_date_start);

        $newformat = date('Y-m-d',$time);

        if($request->_thanos == 'pending') {
            $color = '#ea4335';
        } else {
            $color = '#00bf4f';
        }     
        
        $userId = $request->userid;
      
        $event  = Event::find($id);
        $event->title = $request->potaena;
        $event->start = $newformat . ' ' . $request->_invisible_id;
        $event->end = $newformat . ' ' . $endtime;
        $event->color = $color;
        $event->status = $request->_thanos;
        $event->user_id =  $userId;
        $event->save();
         
        return redirect('/calendar')->with('update', 'Booked Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event  = Event::find($id);

        if($event == null) 
            return Response()->json([
                'message' => 'error delete.'
            ]); 
        
       $event->delete();

        return Response()->json([
            'message' => 'success delete.'
        ]);      
    }

   
}
