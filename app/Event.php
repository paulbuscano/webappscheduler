<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table ='events';

    protected $fillable = [
        'title','start','end','color','status','user_id'
    ];


    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
   
}
