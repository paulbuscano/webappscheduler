@extends('layouts.template')
@section('title', 'Home')
@section('content')



<div id="wrapper">
	<div class="main-content">
	

		<div class="row small-spacing">

		@if(Auth::user()->role == '0')
			<div class="col-lg-4 col-xs-12">
				<div class="box-content">
					<h4 class="box-title text-info">CoSY Members</h4>
					
					

					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						{{-- <ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul> --}}
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<div class="content widget-stat">
						<div id="traffic-sparkline-chart-1" class="left-content margin-top-15"></div>
						<!-- /#traffic-sparkline-chart-1 -->
						<div class="right-content">
							<h2 class="counter text-info">{{$countUser}}</h2>
							<!-- /.counter -->
							<p class="text text-info">Total Members</p>
							<!-- /.text -->
						</div>
						<!-- .right-content -->
					</div>
					<!-- /.content widget-stat -->
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-lg-4 col-xs-12 -->
		@else

			<div class="col-lg-4 col-xs-12">
				<div class="box-content">
					<h4 class="box-title text-info">Schedules</h4>
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						{{-- <ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul> --}}
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<div class="content widget-stat">
						<div id="traffic-sparkline-chart-1" class="left-content margin-top-15"></div>
						<!-- /#traffic-sparkline-chart-1 -->
						<div class="right-content">
							<h2 class="counter text-info">{{$countUser}}</h2>
							<!-- /.counter -->
							<p class="text text-info">Total Schedules</p>
							<!-- /.text -->
						</div>
						<!-- .right-content -->
					</div>
					<!-- /.content widget-stat -->
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-lg-4 col-xs-12 -->
		@endif
			<div class="col-lg-4 col-xs-12">
				<div class="box-content">
					<h4 class="box-title text-danger">Pending Schedule</h4>
					
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						{{-- <ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul> --}}
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<div class="content widget-stat">
						<div id="traffic-sparkline-chart-3" class="left-content margin-top-10"></div>
						<!-- /#traffic-sparkline-chart-3 -->
						<div class="right-content">
							<h2 class="counter text-danger">{{$countPendings}}</h2>
							<!-- /.counter -->
							<p class="text text-danger">Total Pending</p>
							<!-- /.text -->
						</div>
						<!-- .right-content -->
					</div>
					<!-- /.content widget-stat -->
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-lg-4 col-xs-12 -->
			
			<div class="col-lg-4 col-xs-12">
				<div class="box-content">
					<h4 class="box-title text-success">Approved Schedule</h4>
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						{{-- <ul class="sub-menu">
							<li><a href="#">Action</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul> --}}
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<div class="content widget-stat">
						<div id="traffic-sparkline-chart-2" class="left-content"></div>
						<!-- /#traffic-sparkline-chart-2 -->
						<div class="right-content">
							<h2 class="counter text-success">{{$countApproves}}</h2>
							<!-- /.counter -->
							<p class="text text-success">Total Approved</p>
							<!-- /.text -->
						</div>
						<!-- .right-content -->
					</div>
					<!-- /.content widget-stat -->
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-lg-4 col-xs-12 -->
		</div>
		<!-- /.row small-spacing -->

		<div class="row small-spacing">
		

			<div class="col-lg-12 col-xs-12">
				<div class="box-content">
					@if(Auth::user()->role == '0')
						<h4 class="box-title">Recent Schedules</h4>
					@else
						<h4 class="box-title">My Recent Schedules</h4>
					@endif
					<!-- /.box-title -->
					<div class="dropdown js__drop_down">
						<a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
						{{-- <ul class="sub-menu">
							<li><a href="#">Product</a></li>
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else there</a></li>
							<li class="split"></li>
							<li><a href="#">Separated link</a></li>
						</ul> --}}
						<!-- /.sub-menu -->
					</div>
					<!-- /.dropdown js__dropdown -->
					<table class="table table-striped margin-bottom-10">
						<thead>
							<tr>
								<th style="width:20%;">Name</th>
								<th style="width:30%;">Notes</th>
								<th>Date</th>
								<th>Time</th>
								<th>Status</th>
								@if(Auth::user()->role == '0')
								<th style="width:5%;"></th>
								@endif
							</tr>
						</thead>
						<tbody>
							@foreach ($datas as $data)
								<tr>
								
									<td>{{ $data->user->name }}</td>
							
									
									<td>{{ $data->title }}</td>
									
									<?php $newDate = date('F d, Y', strtotime($data->start));?>
									

									<td><?php echo $newDate; ?></td>

									<?php $newTime = date('h:i A', strtotime($data->start));?>
									<?php $timePlus = strtotime($newTime) + 60*60; ?>
									<?php $timeLast = date('h:i A', $timePlus); ?>
									
									<td><?php echo $newTime . ' - ' . $timeLast; ?></td>

									@if($data->status == 'pending')

									<td class="text-danger">{{ $data->status }}</td>

									@elseif($data->status == 'approved')

									<td class="text-success">{{ $data->status }}</td>

									@endif

									@if(Auth::user()->role == '0')	
											
											
										<td><a id="editers"  data-target="#modal-edit"  data-id="{{ $data->id }}" data-userid="{{ $data->user_id }}" data-mytitle="{{ $data->title }}" data-mytime="{{ $newTime }}" data-mypending="{{ $data->status }}" data-startdate="{{$data->start}}" data-name="{{$data->user->name}}" data-toggle="modal" class="MainNavText"><i class="fa fa-plus-circle"></i></a></td>
										
									@endif
								</tr>
							@endforeach

						</tbody>
					
					</table>
					<!-- /.table -->
					{{ $datas->links() }}
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-lg-6 col-xs-12 -->
		</div>

		
		<!-- /.row -->		
		<footer class="footer">
			<ul class="list-inline">
				<li>2018 © CoSY.</li>
				<li><a href="#">Privacy</a></li>
				<li><a href="#">Terms</a></li>
				<li><a href="#">Help</a></li>
			</ul>
		</footer>
	</div>
	<!-- /.main-content -->
</div><!--/#wrapper -->

{{-- edit delete --}}
{!! Form::open(['route' => ['events.update',1],'id'=>'ediwow','method' => 'patch', 'role' => 'form']) !!}           
    @if(Auth::user()->role == '0')
    <div class="modal fade" id="modal-edit" 
        tabindex="-1" role="dialog" 
        aria-labelledby="modal-edit">
    @else
    <div class="modal fade userKalang" id="modal-edit" 
        tabindex="-1" role="dialog" 
        aria-labelledby="modal-edit">
    @endif
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" 
              data-dismiss="modal" 
              aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" 
            id="modal-event">Booking</h4>
          </div>
          <div class="modal-body">

              <div class="form-group{{ $errors->has('_aydi') ? ' has-error' : '' }}">
                            
                  {!! Form::label('_aydi','Booking id:') !!}
      
                  {!! Form::text('_aydi', old('_aydi'), ['id' => '_aydi','class' => 'form-control', 'disabled']) !!}

                  @if ($errors->has('_aydi'))
                        <span class="help-block">
                            <strong>The Date field is required.</strong>
                        </span>
                  @endif   

		  </div>
		  
		  <div class="form-group{{ $errors->has('pangalan') ? ' has-error' : '' }}">
			{!! Form::label('pangalan','Name:') !!}
			
			{!! Form::text('pangalan', old('pangalan'), ['id' => 'pangalan','class' => 'form-control','disabled']) !!}
			{!! Form::hidden('userid', old('userid'), ['id' => 'userid','class' => 'form-control']) !!}
			@if ($errors->has('pangalan'))
				<span class="help-block">
					<strong>The Notes field is required.</strong>
				</span>
			@endif
		  </div>
            
              <div class="form-group{{ $errors->has('_date_start') ? ' has-error' : '' }}">
                            
                      {!! Form::label('_date_start','Date: (DD/MM/YYYY)') !!}
          
                      {!! Form::date('_date_start', null, ['id' => '_date_start','class' => 'form-control shitmen']) !!}

                      @if ($errors->has('_date_start'))
                            <span class="help-block">
                                <strong>The Date field is required.</strong>
                            </span>
                      @endif   

              </div>

              <div class="form-group{{ $errors->has('_invisible_id') ? ' has-error' : '' }}">
                    {!! Form::label('_time_start','Time:') !!}
                    {!! Form::time('_invisible_id', old('_invisible_id'), ['id' => '_invisible_id','class' => 'form-control tatataTime']) !!}
                      @if ($errors->has('_invisible_id'))
                        <span class="help-block">
                            <strong>The Time field is required.</strong>
                        </span>
                      @endif   
                
              </div>
              <div class="form-group{{ $errors->has('_thanos') ? ' has-error' : '' }}">
                    {!! Form::label('status','Status:') !!}
                    
                    <select class="form-control" id="_thanos" name="_thanos" placeholder="Please Select">
                      <option value="pending" {{ old('_thanos') == 'pending' ? 'selected' : '' }}>
                        Pending
                      </option>
                      <option value="approved" {{ old('_thanos') == 'approved' ? 'selected' : '' }}>
                        Approved
                      </option>
                    </select>

                    @if ($errors->has('_thanos'))
                      <span class="help-block">
                          <strong>The Status field is required.</strong>
                      </span>
                    @endif   

                  
            </div>
            <div class="form-group{{ $errors->has('potaena') ? ' has-error' : '' }}">
                  {!! Form::label('potaena','Notes:') !!}
                  {!! Form::textarea('potaena', null, ['id' => 'potaena','class' => 'form-control']) !!}
                  @if ($errors->has('potaena'))
                      <span class="help-block">
                          <strong>The Notes field is required.</strong>
                      </span>
                  @endif 
              </div>
          </div>
          <div class="modal-footer">
          
          
            <button type="button" 
              class="btn btn-default btn-modal" 
              data-dismiss="modal">Close</button>

              <a class="btn btn-modal btn-danger waves-effect waves-light" id="deleteThis" data-href="{{ url('events') }}" data-id="">Delete</a>
            <span class="pull-right">
            {!! Form::submit('Update', ['id' => 'update', 'class' => 'btn btn-warning waves-effect waves-light']) !!}
            </span>
          </div>
        </div>
      </div>
    </div>
{!! Form::close() !!}


@endsection