<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Full Calendar</title>
        {!! Html::style('css/app.css') !!}
        {!! Html::style('vendor/seguce92/fullcalendar/fullcalendar.min.css') !!}
      
        <script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>

        <style>

            body {
              margin: 40px 10px;
              padding: 0;
              font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
              font-size: 14px;
            }
          
            #calendar {
              max-width: 900px;
              margin: 0 auto;
            }
            .fc-slats tr {
               height: 40px;
            }
          
          </style>

    </head>
    <body>
        <div class="container">
            <div class="panel panel-primary">
              <div class="panel-heading"> Add</div>
                <div class="panel-body">
                  {!! Form::open(array('route' => 'events.store','method' => 'POST', 'files' => 'true')) !!}
                  <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12">
                      @if (Session::has('success'))
                      <div class="alert alert=success">
                        {{ Session::get('success') }}
                      </div>
                      @elseif (Session::has('warning'))
                      <div class="alert alert-danger">
                        {{ Session::get('warning') }}
                      </div>
                      @endif
                      
                    </div>

                    <div class="col-xs-4 col-sm-4 col-md-4">
                      <div class="form-group">
                        {!! Form::label('title','Title:') !!}
                        <div class="">
                          {!! Form::text('title', null, ['id' => 'title', 'class' => 'form-control']) !!}
                          {!! $errors->first('title','<p class="alert alert=danger">:message</p>') !!}
                        </div> 
                      </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <div class="form-group">
                          {!! Form::label('date_start','Start Date:') !!}
                          <div class="">
                            {!! Form::date('date_start', null, ['id' => 'umpisa','class' => 'form-control']) !!}
                            {!! $errors->first('date_start','<p class="alert alert=danger">:message</p>') !!}
                          </div> 
                        </div>
                    </div>
                    <div class="col-xs-3 col-sm-3 col-md-3">
                        <div class="form-group">
                          {!! Form::label('time_start','Time Start:') !!}
                          <div class="">
                            {!! Form::time('time_start', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('time_start','<p class="alert alert=danger">:message</p>') !!}
                          </div> 
                        </div>
                    </div>

                    <div class="col-xs-1 col-sm-1 col-md-1 text-center"><br/>
                      {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                      
                    </div>

                  </div>
                  {!! Form::close() !!}
                </div>
            </div>
          </div>
          
        <div class="container">
         
{{-- {!! Form::open(['route' => 'events.store','method' => 'post', 'role' => 'form']) !!}
<div class="modal fade" id="favoritesModal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel">The Sun Also Rises</h4>
      </div>
      <div class="modal-body">
          <div class="form-group">
              {!! Form::label('title','Title:') !!}
              {!! Form::text('title', old('title'), ['id' => 'title','class' => 'form-control']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('date_start','Date start:') !!}
              {!! Form::text('date_start', null, ['id' => 'date_start','class' => 'form-control']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('date_end','Date end:') !!}
              {!! Form::text('date_end', old('date_end'), ['id' => 'date_end','class' => 'form-control']) !!}
          </div>
          <div class="form-group">
              {!! Form::label('time_start','Time start:') !!}
              {!! Form::text('time_start', old('time_start'), ['id' => 'time_start','class' => 'form-control']) !!}
              {!! Form::hidden('invisible_id', old('invisible_id'), ['id' => 'invisible_id','class' => 'form-control']) !!}
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">Close</button>
        <span class="pull-right">
         {!! Form::submit('GUARDAR', ['class' => 'btn btn-success']) !!}
        </span>
      </div>
    </div>
  </div>
</div>
{!! Form::close() !!} --}}
                
  <div id='calendar'></div>
</div>
      
       
    </body>
    {!! Html::script('js/app.js') !!}

    {!! Html::script('vendor/seguce92/fullcalendar/lib/moment.min.js') !!}

    {!! Html::script('vendor/seguce92/fullcalendar/fullcalendar.min.js') !!}

    <script>

      var BASEURL = "{{ url('/') }}";

            $(document).ready(function() {


              var d = new Date();
              var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
            
              var shit;

              $('#calendar').fullCalendar({
                
                selectable: true,

          

              select: function(start, end, jsEvent, view) {  
                
                  if(start.isBefore(moment())) {
                      $('#calendar').fullCalendar('unselect');
                      return false;
                  } else {

                    // alert('pota: ' + start.format('YYYY-MM-DD'));
                    // console.log(start.format());

                  
                  $('.modal-body #date_start').val(start.format('YYYY-MM-DD'));
                  $('.modal-body #date_end').val(start.format('YYYY-MM-DD'));
                  $('.modal-body #time_start').val(start.format('h:mm A'));

                  $('.modal-body #invisible_id').val(start.format('HH:mm:ss'));
                  $('#favoritesModal').modal('show'); 

                  // alert('pota: ' + start.format());

                  }      

              },

              // selectOverlap: function(event) {
              //       return !event.block;
              // },
           
              eventClick: function(calEvent, jsEvent, view) {

                  alert('Event: ' + calEvent.title);

                  alert('Date: ' + calEvent.start.format());
                  alert('Time: ' + calEvent.start.format('h:mm A'));
                  // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);
                  

                  


              },	

                eventRender: function(event, element, view) {
                var eventEnd = moment(event.end);
                var NOW = moment();
                if (eventEnd.diff(NOW, 'seconds') <= 0) {
                    return false;
                }},

                viewRender: function(currentView){
                var minDate = moment(),
                maxDate = moment().add(100,'weeks');
                // Past
                if (minDate >= currentView.start && minDate <= currentView.end) {
                  $(".fc-prev-button").prop('disabled', true); 
                  $(".fc-prev-button").addClass('fc-state-disabled'); 
                }
                else {
                  $(".fc-prev-button").removeClass('fc-state-disabled'); 
                  $(".fc-prev-button").prop('disabled', false); 
                }
                // Future
                if (maxDate >= currentView.start && maxDate <= currentView.end) {
                  $(".fc-next-button").prop('disabled', true); 
                  $(".fc-next-button").addClass('fc-state-disabled'); 
                } else {
                  $(".fc-next-button").removeClass('fc-state-disabled'); 
                  $(".fc-next-button").prop('disabled', false); 
                }
              },

   

                defaultDate: strDate,
                selectHelper: true,
                selectable: true,
                defaultView: 'agendaWeek',
                minTime: '06:00:00',
                maxTime: '24:00:00',
                forceEventDuration : true,
                slotDuration: '01:00:00', 
                navLinks: false, // can click day/week names to navigate views
                editable: false,
                allDaySlot: false,
                eventOverlap: false,
                timeFormat: 'h:mm t',
                eventDurationEditable: false,
                eventLimit: false, // allow "more" link when too many events
                events: BASEURL + '/events'
              });
     
            });

            

    
          </script>
</html>
