@extends('layouts.app')

@section('content')

        <form  class="frm-single" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
            <div class="inside">
                <div class="title login-title"><a href="http://cosy.ph" alt="cosy"><img src="{{ asset('images/logo.png') }}" alt=""></a></div>
    
                <!-- /.title -->
                <div class="frm-title">Member Login</div>
                <!-- /.frm-title -->
                <div class="frm-input{{ $errors->has('email') ? ' has-error' : '' }}"><input id="email" type="email" placeholder="Email Address" value="{{ old('email') }}" class="frm-inp" name="email" value="{{ old('email') }}" required autofocus><i class="fa fa-user frm-ico"></i> 
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <!-- /.frm-input -->
                <div class="frm-input{{ $errors->has('password') ? ' has-error' : '' }}"><input id="password" type="password" placeholder="Password" class="frm-inp" name="password" required><i class="fa fa-lock frm-ico"></i>
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
                <!-- /.frm-input -->
                {{-- <div class="clearfix margin-bottom-20">
                    
                    <!-- /.pull-left -->
                    <div class="text-center"><a href="page-recoverpw.html" class="a-link"><i class="fa fa-unlock-alt"></i>Forgot password?</a></div>
                    <!-- /.pull-right -->
                </div> --}}
                <!-- /.clearfix -->
                <button type="submit" class="frm-submit">Login<i class="fa fa-arrow-circle-right"></i></button>
                <!-- <div class="row small-spacing">
                    <div class="col-sm-12">
                        <div class="txt-login-with txt-center">or login with</div>
                        
                    </div>
                    
                    <div class="col-sm-6"><button type="button" class="btn btn-sm btn-icon btn-icon-left btn-social-with-text btn-facebook text-white waves-effect waves-light"><i class="ico fa fa-facebook"></i><span>Facebook</span></button></div>
                    
                    <div class="col-sm-6"><button type="button" class="btn btn-sm btn-icon btn-icon-left btn-social-with-text btn-google-plus text-white waves-effect waves-light"><i class="ico fa fa-google-plus"></i>Google+</button></div>
                    
                </div> -->
                <!-- /.row -->
                
                <div class="frm-footer">CoSY © 2018.</div>
                <!-- /.footer -->
            </div>
            <!-- .inside -->
        </form>
        <!-- /.frm-single -->



@endsection
