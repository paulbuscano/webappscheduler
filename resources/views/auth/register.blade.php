@extends('layouts.template')
@section('title', 'Register User')
@section('content')
<div id="wrapper">
<div class="main-content">


<div class="row small-spacing">
		

    <div class="col-lg-12 col-xs-12">
        <div class="box-content">
                                    <h4 class="box-title">Recent Schedules</h4>
                                <!-- /.box-title -->
            <div class="dropdown js__drop_down">
                <a href="#" class="dropdown-icon glyphicon glyphicon-option-vertical js__drop_down_button"></a>
                
                <!-- /.sub-menu -->
            </div>
            <!-- /.dropdown js__dropdown -->
            <table class="table table-striped margin-bottom-10">
                <thead>
                    <tr>
                        <th style="width:20%;">User Id</th>
                        <th style="width:20%;">Name</th>
                        <th>Email</th>
                        <th>Date Added</th>
                        <th>Role</th>
                        <th class="text-center">Status</th>
                        <th style="width:5%;"></th>
                        </tr>
                </thead>
                    <tbody>
                        @foreach ($users as $user)
                            <tr>
                                
                                <td>{{ $user->id }}</td>
                                
                                <td>{{ $user->name }}</td>
                                
        
                                
                                <td>{{ $user->email }}</td>
                                <td><?php echo $newDate = date('F d, Y', strtotime($user->created_at));?></td>
                                @if($user->role == 0) 
                                    <td>Admin</td>
                                @else
                                    <td>User</td>
                                @endif
                               
                                @if($user->active == true) 
                                <td class="text-center text-success">Active</td>
                                @else
                                <td class="text-center text-danger">Deactivated</td>
                                @endif
         
                                <td><a id="userEditers"  data-target="#modal-user-edit"  data-status="{{ $user->active }}" data-id="{{ $user->id }}" data-name="{{ $user->name }}" data-email="{{ $user->email }}" data-role="{{ $user->role }}"  data-toggle="modal" class="MainNavText"><i class="fa fa-plus-circle"></i></a</td>                           
                            </tr>
                        @endforeach

                    </tbody>
            
            </table>
            <!-- /.table -->

            {{ $users->links() }}
        </div>
        <!-- /.box-content -->
    </div>
    <!-- /.col-lg-6 col-xs-12 -->
</div>



<div class="row small-spacing">
    <div class="col-lg-12 col-xs-12">
        <div>
            <div class="panel panel-default">
                <div class="panel-heading">Add new user</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
                            <label for="role" class="col-md-4 control-label">Role</label>

                            <div class="col-md-6">

                                <select class="form-control" id="role" name="role" placeholder="Please Select">
                                        <option value="0" {{ old('role') == 'pending' ? 'selected' : '' }}>
                                          Admin
                                        </option>
                                        <option value="1" {{ old('role') == 'approved' ? 'selected' : '' }}>
                                          User
                                        </option>
                                </select>
                                @if ($errors->has('role'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('role') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>

{{-- edit delete --}}
{!! Form::open(['route' => ['user.update',1],'id'=>'ediwowUser','method' => 'patch', 'role' => 'form']) !!}           
    <div class="modal fade" id="modal-user-edit" 
        tabindex="-1" role="dialog" 
        aria-labelledby="modal-user-edit">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" 
              data-dismiss="modal" 
              aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" 
            id="modal-event">Booking</h4>
          </div>
          <div class="modal-body">

              <div class="form-group{{ $errors->has('user_id') ? ' has-error' : '' }}">
                            
                  {!! Form::label('user_id','User id:') !!}
      
                  {!! Form::text('user_id', old('user_id'), ['id' => 'user_id','class' => 'form-control', 'disabled']) !!}

                  @if ($errors->has('user_id'))
                        <span class="help-block">
                            <strong>The User Id field is required.</strong>
                        </span>
                  @endif   

          </div>
            
              <div class="form-group{{ $errors->has('userName') ? ' has-error' : '' }}">
                            
                      {!! Form::label('userName','Name:') !!}
          
                      {!! Form::text('userName', null, ['id' => 'userName','class' => 'form-control shitmen']) !!}

                      @if ($errors->has('userName'))
                            <span class="help-block">
                                <strong>The Name field is required.</strong>
                            </span>
                      @endif   

              </div>

              <div class="form-group{{ $errors->has('userEmail') ? ' has-error' : '' }}">
                    {!! Form::label('userEmail','Email:') !!}
                    {!! Form::email('userEmail', old('userEmail'), ['id' => 'userEmail','class' => 'form-control tatataTime']) !!}
                      @if ($errors->has('userEmail'))
                        <span class="help-block">
                            <strong>The Email field is required.</strong>
                        </span>
                      @endif   
                
              </div>
              <div class="form-group{{ $errors->has('userPassword') ? ' has-error' : '' }}">
                {!! Form::label('userPassword','Password:') !!}
                {!! Form::password('userPassword', ['class' => 'form-control']) !!}
                  @if ($errors->has('userPassword'))
                    <span class="help-block">
                        <strong>The Password field is required.</strong>
                    </span>
                  @endif   
            
            </div>

            <div class="form-group{{ $errors->has('passConfirm') ? ' has-error' : '' }}">
                {!! Form::label('passConfirm','Confirm Password:') !!}
                {!! Form::password('passConfirm', ['class' => 'form-control']) !!}
                  @if ($errors->has('passConfirm'))
                    <span class="help-block">
                        <strong>The Password field is required.</strong>
                    </span>
                  @endif   
            
            </div>

            <div class="form-group{{ $errors->has('userRole') ? ' has-error' : '' }}">
                  {!! Form::label('userRole','Role:') !!}

                  <select class="form-control" id="userRole" name="userRole" placeholder="Please Select">
                    <option value="0" {{ old('userRole') == 'pending' ? 'selected' : '' }}>
                      Admin
                    </option>
                    <option value="1" {{ old('userRole') == 'approved' ? 'selected' : '' }}>
                      User
                    </option>
                  </select>

                  @if ($errors->has('userRole'))
                      <span class="help-block">
                          <strong>The Notes field is required.</strong>
                      </span>
                  @endif 
              </div>
              <div class="form-group{{ $errors->has('userStatus') ? ' has-error' : '' }}">
                    {!! Form::label('userStatus','Status:') !!}
  
                    <select class="form-control" id="userStatus" name="userStatus" placeholder="Please Select">
                      <option value="0" {{ old('userStatus') == '0' ? 'selected' : '' }}>
                        Deactivate
                      </option>
                      <option value="1" {{ old('userStatus') == '1' ? 'selected' : '' }}>
                        Active
                      </option>
                    </select>
  
                    @if ($errors->has('userStatus'))
                        <span class="help-block">
                            <strong>The Notes field is required.</strong>
                        </span>
                    @endif 
                </div>
          </div>
          <div class="modal-footer">
          
          
            <button type="button" 
              class="btn btn-default btn-modal" 
              data-dismiss="modal">Close</button>

             
              <a class="btn btn-modal btn-danger waves-effect waves-light" id="deleteUser" data-href="{{ url('user') }}" data-id="">Delete</a>
            

              <span class="pull-right">
            {!! Form::submit('Update', ['id' => 'update', 'class' => 'btn btn-warning waves-effect waves-light']) !!}
            </span>
          </div>
        </div>
      </div>
    </div>
{!! Form::close() !!}



@endsection
