@extends('layouts.template')
@section('title', 'Booking')
@section('content')
<div id="wrapper">
	<div class="main-content">
           
		<div class="row">
    @if(Auth::user()->role == '0')
			<div class="col-md-3">
				<a href="#"  data-toggle="modal" data-target="#favoritesModal" class="btn btn-lg btn-success btn-block waves-effect waves-light btn-submitter">
					<i class="fa fa-plus"></i> Create New
				</a>
				<div id="external-events" class="margin-top-30">
					<p style="text-align: center;">Status</p>
					<div style="cursor: default;" class="fc-event bg-success">Approved</div>
					<!-- /.fc-event bg-success -->
					<div style="cursor: default;" class="fc-event bg-danger">Pending</div>
								
				</div>
				<!-- /#external-events.margin-top-20 -->
			</div>
            <!-- /.col-md-3 -->
    @endif
            
      @if(Auth::user()->role == '1')
			<div class="col-lg-12">
      @endif

      @if(Auth::user()->role == '0')
			<div class="col-md-9">
      @endif

				<div class="box-content">
          
          <div id="calendar"></div>
          
					<!-- /#calendar -->
				</div>
				<!-- /.box-content -->
			</div>
			<!-- /.col-md-9 -->
		</div>
		<!-- /.row -->		
		<footer class="footer">
			<ul class="list-inline">
				<li>2018 © CoSY.</li>
				<li><a href="#">Privacy</a></li>
				<li><a href="#">Terms</a></li>
				<li><a href="#">Help</a></li>
			</ul>
		</footer>
	</div>
	<!-- /.main-content -->
</div><!--/#wrapper -->

{{-- add --}}
{!! Form::open(['route' => 'events.store','method' => 'post', 'role' => 'form']) !!}
{{ csrf_field() }}
  @if(Auth::user()->role == '0')
    <div class="modal fade" id="favoritesModal" 
          tabindex="-1" role="dialog" 
          aria-labelledby="favoritesModalLabel">
  @else
    <div class="modal fade userKalang" id="favoritesModal" 
          tabindex="-1" role="dialog" 
          aria-labelledby="favoritesModalLabel">
  @endif
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" 
              data-dismiss="modal" 
              aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" 
            id="favoritesModalLabel">Booking</h4>
          </div>
          <div class="modal-body">
              
              <div class="form-group{{ $errors->has('date_start') ? ' has-error' : '' }}">
                  
                  @if(Auth::user()->role == '0')
                      {!! Form::label('date_start','Date: (DD/MM/YYYY)') !!}
                  @else 
                      {!! Form::label('date_start','Date:') !!}
                  @endif   
                  
                  @if(Auth::user()->role == '0')
                      {!! Form::date('date_start', null, ['id' => 'date_start','class' => 'form-control']) !!}
                      
                      @if ($errors->has('date_start'))
                            <span class="help-block">
                                <strong>The Date field is required.</strong>
                            </span>
                      @endif   
                                      
                  @else 
                      {!! Form::text('date_start_show', null, ['id' => 'date_start_show','class' => 'form-control', 'disabled']) !!}
                      {!! Form::hidden('date_start', old('date_start'), ['id' => 'date_start','class' => 'form-control']) !!}
                      @if ($errors->has('date_start'))
                            <span class="help-block">
                                <strong>The Date field is required.</strong>
                            </span>
                      @endif   
                      {!! Form::hidden('date_end', old('date_end'), ['id' => 'date_end','class' => 'form-control']) !!}
                  @endif

              </div>
              {{-- <div class="form-group">
                  {!! Form::label('date_end','Date end:') !!}
                  {!! Form::text('date_end', old('date_end'), ['id' => 'date_end','class' => 'form-control']) !!}
              </div> --}}
              <div class="form-group{{ $errors->has('invisible_id') ? ' has-error' : '' }}">
                    {!! Form::label('time_start','Time:') !!}

                  @if(Auth::user()->role == '0')
                    {!! Form::time('invisible_id', old('invisible_id'), ['id' => 'invisible_id','class' => 'form-control tatataTime']) !!}
                      @if ($errors->has('invisible_id'))
                        <span class="help-block">
                            <strong>The Time field is required.</strong>
                        </span>
                      @endif   
                  @else 
                    {!! Form::text('_time_start', old('_time_start'), ['id' => '_time_start','class' => 'form-control', 'disabled']) !!}
                    {!! Form::hidden('invisible_id', old('invisible_id'), ['id' => 'invisible_id','class' => 'form-control']) !!}
                  @endif
              </div>
              <div class="form-group{{ $errors->has('thanos') ? ' has-error' : '' }}">
                    {!! Form::label('status','Status:') !!}

                    @if(Auth::user()->role == '0')
                    {{-- {{ Form::select('thanos',  ['pending' => 'Pending', 'approved' => 'Approved']) }} --}}
                    
                    <select class="form-control" id="thanos" name="thanos" placeholder="Please Select">
                      <option value="pending" {{ old('thanos') == 'pending' ? 'selected' : '' }}>
                        Pending
                      </option>
                      <option value="approved" {{ old('thanos') == 'approved' ? 'selected' : '' }}>
                        Approved
                      </option>
                    </select>

                    @if ($errors->has('thanos'))
                      <span class="help-block">
                          <strong>The Status field is required.</strong>
                      </span>
                    @endif   

                  
                    @else
                    {!! Form::text('thanos_show', old('thanos_show'), ['id' => 'thanos_show','class' => 'form-control', 'disabled']) !!}
                    {!! Form::hidden('thanos', old('thanos'), ['id' => 'thanos','class' => 'form-control patayKayo']) !!}
                    @endif
            </div>
              <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                  {!! Form::label('title','Notes:') !!}
                  {!! Form::textarea('title', null, ['id' => 'title','class' => 'form-control']) !!}
                  @if ($errors->has('title'))
                      <span class="help-block">
                          <strong>The Notes field is required.</strong>
                      </span>
                  @endif 
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" 
                class="btn btn-default btn-modal" 
                data-dismiss="modal">Close</button>
            <span class="pull-right">
              {!! Form::submit('Submit', ['class' => 'btn btn-success btn-submitter']) !!}
            </span>
          </div>
        </div>
      </div>
    </div>
{!! Form::close() !!}

{{-- edit delete --}}
{!! Form::open(['route' => ['events.update',2],'id'=>'kingkidama','method' => 'patch', 'role' => 'form']) !!}           
    @if(Auth::user()->role == '0')
    <div class="modal fade" id="modal-event" 
        tabindex="-1" role="dialog" 
        aria-labelledby="modal-event">
    @else
    <div class="modal fade userKalang" id="modal-event" 
        tabindex="-1" role="dialog" 
        aria-labelledby="modal-event">
    @endif
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" 
              data-dismiss="modal" 
              aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" 
            id="modal-event">Booking</h4>
          </div>
          <div class="modal-body">

              <div class="form-group{{ $errors->has('_aydi') ? ' has-error' : '' }}">
                            
                  {!! Form::label('_aydi','Booking id:') !!}
      
                  {!! Form::text('_aydi', old('_aydi'), ['id' => '_aydi','class' => 'form-control', 'disabled']) !!}

                  @if ($errors->has('_aydi'))
                        <span class="help-block">
                            <strong>The Date field is required.</strong>
                        </span>
                  @endif   

             </div>

             <div class="form-group{{ $errors->has('pangalan') ? ' has-error' : '' }}">
              {!! Form::label('pangalan','Name:') !!}
              
              {!! Form::text('pangalan', old('pangalan'), ['id' => 'pangalan','class' => 'form-control','disabled']) !!}
              {!! Form::hidden('userid', old('userid'), ['id' => 'userid','class' => 'form-control']) !!}
              @if ($errors->has('pangalan'))
                  <span class="help-block">
                      <strong>The Notes field is required.</strong>
                  </span>
              @endif
            </div>
            
              <div class="form-group{{ $errors->has('_date_start') ? ' has-error' : '' }}">
                            
                      {!! Form::label('_date_start','Date: (DD/MM/YYYY)') !!}
          
                      {!! Form::date('_date_start', null, ['id' => '_date_start','class' => 'form-control shitmen']) !!}

                      @if ($errors->has('_date_start'))
                            <span class="help-block">
                                <strong>The Date field is required.</strong>
                            </span>
                      @endif   

              </div>

              <div class="form-group{{ $errors->has('_invisible_id') ? ' has-error' : '' }}">
                    {!! Form::label('_time_start','Time:') !!}
                    {!! Form::time('_invisible_id', old('_invisible_id'), ['id' => '_invisible_id','class' => 'form-control tatataTime']) !!}
                      @if ($errors->has('_invisible_id'))
                        <span class="help-block">
                            <strong>The Time field is required.</strong>
                        </span>
                      @endif   
                
              </div>
              <div class="form-group{{ $errors->has('_thanos') ? ' has-error' : '' }}">
                    {!! Form::label('status','Status:') !!}
                    
                    <select class="form-control" id="_thanos" name="_thanos" placeholder="Please Select">
                      <option value="pending" {{ old('_thanos') == 'pending' ? 'selected' : '' }}>
                        Pending
                      </option>
                      <option value="approved" {{ old('_thanos') == 'approved' ? 'selected' : '' }}>
                        Approved
                      </option>
                    </select>

                    @if ($errors->has('_thanos'))
                      <span class="help-block">
                          <strong>The Status field is required.</strong>
                      </span>
                    @endif   

                  
            </div>
            <div class="form-group{{ $errors->has('potaena') ? ' has-error' : '' }}">
                  {!! Form::label('potaena','Notes:') !!}
                  {!! Form::textarea('potaena', null, ['id' => 'potaena','class' => 'form-control']) !!}
                  @if ($errors->has('potaena'))
                      <span class="help-block">
                          <strong>The Notes field is required.</strong>
                      </span>
                  @endif 
              </div>

             

         
       
    
          
          </div>
          <div class="modal-footer">
          
          
            <button type="button" 
              class="btn btn-default btn-modal" 
              data-dismiss="modal">Close</button>

              <a class="btn btn-modal btn-danger waves-effect waves-light" id="delete" data-href="{{ url('events') }}" data-id="">Delete</a>
            <span class="pull-right">
            {!! Form::submit('Update', ['id' => 'update', 'class' => 'btn btn-warning waves-effect waves-light']) !!}
            </span>
          </div>
        </div>
      </div>
    </div>
{!! Form::close() !!}

@endsection