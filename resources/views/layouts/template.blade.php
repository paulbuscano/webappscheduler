<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>CoSY - @yield('title', 'Welcome')</title>

	<!-- Main Styles -->
	<link rel="stylesheet" href="{{ URL::asset('styles/style.min.css') }}">
	
	<!-- Themify Icon -->
	<link rel="stylesheet" href="{{ URL::asset('fonts/themify-icons/themify-icons.css') }}">

	<!-- mCustomScrollbar -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/mCustomScrollbar/jquery.mCustomScrollbar.min.css') }}">

	<!-- Waves Effect -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/waves/waves.min.css') }}">

	<!-- Sweet Alert -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/sweet-alert/sweetalert.css') }}">
	
	<!-- Percent Circle -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/percircle/css/percircle.css') }}">

	<!-- Chartist Chart -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/chart/chartist/chartist.min.css') }}">

	<!-- FullCalendar -->
	<link rel="stylesheet" href="{{ URL::asset('plugin/fullcalendar/fullcalendar.min.css') }}">
	<link rel="stylesheet" href="{{ URL::asset('plugin/fullcalendar/fullcalendar.print.css') }}" media='print'>
	
	
</head>

@if(Auth::user()->role == '0')
<style>
	.fc .fc-event, #external-events .fc-event {
		margin: 0!important;
		border-radius: 0!important;
		border: 1px solid #fff;
    	border-left: 0;
    	border-right: 0;
		border-color: #fff !important;
	}

</style>

@else
<style>
	.fc .fc-event, #external-events .fc-event {
		margin: 0!important;
		border-radius: 0!important;
		border: 1px solid #fff;
    	border-left: 0;
    	border-right: 0;
		
	}

	.fc-title {
   		 display: none;
	}

	.fc .fc-event, .fc .fc-event-dot {
	background: #010101 !important;
	border-color: #fff !important;
   
	}

</style>
@endif

<body>
<div class="main-menu">
	<header class="header">
		<a href="/" class="logo">CoSY</a>
		<button type="button" class="button-close fa fa-times js__menu_close"></button>
	</header>
	<!-- /.header -->
	<div class="content">

		<div class="navigation">
			<h5 class="title">Navigation</h5>
			
			<!-- /.title -->
			<ul class="menu js__accordion">
				<li  class="{{ Request::is('/*') ? 'current' : '' }}">
					<a class="waves-effect" href="/"><i class="menu-icon ti-dashboard"></i><span>Dashboard</span></a>
				</li>
				
				<li  class="{{ Request::is('calendar*') ? 'current' : '' }}">
					<a class="waves-effect" href="calendar"><i class="menu-icon ti-calendar"></i><span>Calendar Booking</span></a>
				</li>
				@if(Auth::user()->role == '0')
				<li class="{{ Request::is('register*') ? 'current' : '' }}">
					<a class="waves-effect" href="register"><i class="menu-icon ti-user"></i><span>Users</span></a>
				</li>
				@endif
				{{-- <li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-bar-chart"></i><span>Charts</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="chart-3d.html">3D Charts</a></li>
						<li><a href="chart-chartist.html">Chartist Charts</a></li>
						<li><a href="chart-chartjs.html">Chartjs Chart</a></li>
						<li><a href="chart-dynamic.html">Dynamic Chart</a></li>
						<li><a href="chart-flot.html">Flot Chart</a></li>
						<li><a href="chart-knob.html">Knob Chart</a></li>
						<li><a href="chart-morris.html">Morris Chart</a></li>
						<li><a href="chart-sparkline.html">Sparkline Chart</a></li>
						<li><a href="chart-other.html">Other Chart</a></li>
						
					</ul>
					<!-- /.sub-menu js__content -->
				</li> --}}
				{{-- <li>
					<a class="waves-effect" href="widgets.html"><i class="menu-icon ti-layers-alt"></i><span>Widgets</span><span class="notice notice-yellow">6</span></a>
				</li> --}}
			</ul>
			<!-- /.menu js__accordion -->
			{{-- <h5 class="title">User Interface</h5>
			<!-- /.title -->
			<ul class="menu js__accordion">
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-flag"></i><span>Icons</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="icons-font-awesome-icons.html">Font Awesome</a></li>
						<li><a href="icons-fontello.html">Fontello</a></li>
						<li><a href="icons-material-icons.html">Material Design Icons</a></li>
						<li><a href="icons-material-design-iconic.html">Material Design Iconic Font</a></li>
						<li><a href="icons-themify-icons.html">Themify Icons</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-desktop"></i><span>UI Kit</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="ui-buttons.html">Buttons</a></li>
						<li><a href="ui-cards.html">Cards</a></li>
						<li><a href="ui-checkbox-radio.html">Checkboxs-Radios</a></li>
						<li><a href="ui-components.html">Components</a></li>
						<li><a href="ui-draggable-cards.html">Draggable Cards</a></li>
						<li><a href="ui-modals.html">Modals</a></li>
						<li><a href="ui-notification.html">Notification</a></li>
						<li><a href="ui-range-slider.html">Range Slider</a></li>
						<li><a href="ui-sweetalert.html">Sweet Alert</a></li>
						<li><a href="ui-treeview.html">Tree view</a></li>
						<li><a href="ui-typography.html">Typography</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-layout"></i><span>Forms</span><span class="notice notice-blue">7</span></a>
					<ul class="sub-menu js__content">
						<li><a href="form-elements.html">General Elements</a></li>
						<li><a href="form-advanced.html">Advanced Form</a></li>
						<li><a href="form-fileupload.html">Form Uploads</a></li>
						<li><a href="form-validation.html">Form Validation</a></li>
						<li><a href="form-wizard.html">Form Wizard</a></li>
						<li><a href="form-wysiwig.html">Wysiwig Editors</a></li>
						<li><a href="form-xeditable.html">X-editable</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-layout-accordion-merged"></i><span>Tables</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="tables-basic.html">Basic Tables</a></li>
						<li><a href="tables-datatable.html">Data Tables</a></li>
						<li><a href="tables-responsive.html">Responsive Tables</a></li>
						<li><a href="tables-editable.html">Editable Tables</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
			</ul>
			<!-- /.menu js__accordion -->
			<h5 class="title">Additions</h5>
			<!-- /.title -->
			<ul class="menu js__accordion">
				<li>
					<a class="waves-effect" href="profile.html"><i class="menu-icon ti-user"></i><span>Profile</span></a>
				</li>
				<li>
					<a class="waves-effect" href="inbox.html"><i class="menu-icon ti-email"></i><span>Mail</span><span class="notice notice-danger">New</span></a>
				</li>
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-layers"></i><span>Page</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="page-starter.html">Starter Page</a></li>
						<li><a href="page-login.html">Login</a></li>
						<li><a href="page-register.html">Register</a></li>
						<li><a href="page-recoverpw.html">Recover Password</a></li>
						<li><a href="page-lock-screen.html">Lock Screen</a></li>
						<li><a href="page-confirm-mail.html">Confirm Mail</a></li>
						<li><a href="page-404.html">Error 404</a></li>
						<li><a href="page-500.html">Error 500</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
				<li>
					<a class="waves-effect parent-item js__control" href="#"><i class="menu-icon ti-blackboard"></i><span>Extra Pages</span><span class="menu-arrow fa fa-angle-down"></span></a>
					<ul class="sub-menu js__content">
						<li><a href="extras-contact.html">Contact list</a></li>
						<li><a href="extras-email-template.html">Email template</a></li>
						<li><a href="extras-faq.html">FAQ</a></li>
						<li><a href="extras-gallery.html">Gallery</a></li>
						<li><a href="extras-invoice.html">Invoice</a></li>
						<li><a href="extras-maps.html">Maps</a></li>
						<li><a href="extras-pricing.html">Pricing</a></li>
						<li><a href="extras-projects.html">Projects</a></li>
						<li><a href="extras-taskboard.html">Taskboard</a></li>
						<li><a href="extras-timeline.html">Timeline</a></li>
						<li><a href="extras-tour.html">Tour</a></li>
					</ul>
					<!-- /.sub-menu js__content -->
				</li>
			</ul> --}}
			<!-- /.menu js__accordion -->
		</div>
		<!-- /.navigation -->
	</div>
	<!-- /.content -->
</div>
<!-- /.main-menu -->

<div class="fixed-navbar">
	<div class="pull-left">
		<button type="button" class="menu-mobile-button glyphicon glyphicon-menu-hamburger js__menu_mobile"></button>
		
		<h1 class="page-title">Welcome back, {{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->email }}}!</h1>
		<!-- /.page-title -->
	</div>
	<!-- /.pull-left -->
	<div class="pull-right">
		
		<!-- /.ico-item -->
		{{-- <a href="#" class="ico-item ti-email notice-alarm js__toggle_open" data-target="#message-popup"></a>
		<a href="#" class="ico-item ti-bell notice-alarm js__toggle_open" data-target="#notification-popup"></a> --}}
		<div class="ico-item">
			<i class="ti-user"></i>
			<ul class="sub-ico-item">
				<li><a href="register">Settings</a></li>
				{{-- <li><a class="js__logout" href="#">Log Out</a></li> --}}
				<li>
					<a href="{{ route('logout') }}"
							onclick="event.preventDefault();
									 document.getElementById('logout-form').submit();">
							Logout
					</a>
	
					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
						{{ csrf_field() }}
					</form>
				</li>
			</ul>
			<!-- /.sub-ico-item -->
		</div>
	</div>
	<!-- /.pull-right -->
</div>
<!-- /.fixed-navbar -->

<div id="notification-popup" class="notice-popup js__toggle" data-space="75">
	
	<h2 class="popup-title">Your Notifications</h2>
	<!-- /.popup-title -->
	<div class="content">
		<ul class="notice-list">
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-1.jpg') }}" alt=""></span>
					<span class="name">John Doe</span>
					<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
					<span class="time">10 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-2.jpg') }}" alt=""></span>
					<span class="name">Anna William</span>
					<span class="desc">Like your post: “Facebook Messenger”</span>
					<span class="time">15 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar bg-warning"><i class="fa fa-warning"></i></span>
					<span class="name">Update Status</span>
					<span class="desc">Failed to get available update data. To ensure the please contact us.</span>
					<span class="time">30 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-1.jpg') }}" alt=""></span>
					<span class="name">Jennifer</span>
					<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
					<span class="time">45 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-6.jpg') }}" alt=""></span>
					<span class="name">Michael Zenaty</span>
					<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
					<span class="time">50 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-4.jpg') }}" alt=""></span>
					<span class="name">Simon</span>
					<span class="desc">Like your post: “Facebook Messenger”</span>
					<span class="time">1 hour</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar bg-violet"><i class="fa fa-flag"></i></span>
					<span class="name">Account Contact Change</span>
					<span class="desc">A contact detail associated with your account has been changed.</span>
					<span class="time">2 hours</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-7.jpg') }}" alt=""></span>
					<span class="name">Helen 987</span>
					<span class="desc">Like your post: “Facebook Messenger”</span>
					<span class="time">Yesterday</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-2.jpg') }}" alt=""></span>
					<span class="name">Denise Jenny</span>
					<span class="desc">Like your post: “Contact Form 7 Multi-Step”</span>
					<span class="time">Oct, 28</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-8.jpg') }}" alt=""></span>
					<span class="name">Thomas William</span>
					<span class="desc">Like your post: “Facebook Messenger”</span>
					<span class="time">Oct, 27</span>
				</a>
			</li>
		</ul>
		<!-- /.notice-list -->
		<a href="#" class="notice-read-more">See more messages <i class="fa fa-angle-down"></i></a>
	</div>
	<!-- /.content -->
</div>
<!-- /#notification-popup -->

<div id="message-popup" class="notice-popup js__toggle" data-space="75">
	<h2 class="popup-title">Recent Messages<a href="#" class="pull-right text-danger">New message</a></h2>
	<!-- /.popup-title -->
	<div class="content">
		<ul class="notice-list">
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-1.jpg') }}" alt=""></span>
					<span class="name">John Doe</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">10 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-3.jpg') }}" alt=""></span>
					<span class="name">Harry Halen</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">15 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-4.jpg') }}" alt=""></span>
					<span class="name">Thomas Taylor</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">30 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-1.jpg') }}" alt=""></span>
					<span class="name">Jennifer</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">45 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-sm-5.jpg') }}" alt=""></span>
					<span class="name">Helen Candy</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">45 min</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-2.jpg') }}" alt=""></span>
					<span class="name">Anna Cavan</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">1 hour ago</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar bg-success"><i class="fa fa-user"></i></span>
					<span class="name">Jenny Betty</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">1 day ago</span>
				</a>
			</li>
			<li>
				<a href="#">
					<span class="avatar"><img src="{{ asset('images/avatar-5.jpg') }}" alt=""></span>
					<span class="name">Denise Peterson</span>
					<span class="desc">Amet odio neque nobis consequuntur consequatur a quae, impedit facere repellat voluptates.</span>
					<span class="time">1 year ago</span>
				</a>
			</li>
		</ul>
		<!-- /.notice-list -->
		<a href="#" class="notice-read-more">See more messages <i class="fa fa-angle-down"></i></a>
	</div>
	<!-- /.content -->
</div>
<!-- /#message-popup -->
@yield('content')


</body>


{!! Html::script('js/app.js') !!}

{{-- {!! Html::script('vendor/seguce92/fullcalendar/lib/moment.min.js') !!} --}}

{{-- {!! Html::script('vendor/seguce92/fullcalendar/fullcalendar.min.js') !!} --}}

<script>

    var BASEURL = "{{ url('/') }}";

          $(document).ready(function() {

            var d = new Date();
            var strDate = d.getFullYear() + "/" + (d.getMonth()+1) + "/" + d.getDate();
          
            var shit;

            $('#calendar').fullCalendar({
              
              selectable: true,

            select: function(start, end, jsEvent, view) {  
              
                if(start.isBefore(moment())) {
					$('#calendar').fullCalendar('unselect');
					 
                    return false;
                } else {

				  
				if($('.tatataTime').length) {
					$('.modal-body #invisible_id').val(start.format('HH:mm:ss'));
				} else {
					
					$('.modal-body #invisible_id').val(start.format('HH:mm:ss'));
				}

                if($('.userKalang').length) {
					$('.modal-body #_time_start').val(start.format('h:mm A') + ' - ' + moment.utc(start,'h:mm A').add(1,'hour').format('hh:mm A'));
					$('.modal-body #date_start').val(start.format('YYYY-MM-DD'));
					$('.modal-body #date_start_show').val(start.format('LL'));
					
				} else {
					$('.modal-body #date_start').val(start.format('YYYY-MM-DD'));
					
				}
                
				if($('.patayKayo').length) {
					$('.modal-body #thanos').val('pending');
					$('.modal-body #thanos_show').val('pending');
				} else {
					$('.modal-body #thanos').val(start.status);

				}
				$('.modal-body #date_end').val(start.format('YYYY-MM-DD'));
			
            
            	 //   $('.modal-body #invisible_id').val(start.format('HH:mm:ss'));
                $('#favoritesModal').modal('show'); 

				
                // alert('pota: ' + start.format());

                }      

            },
			
            eventClick: function(calEvent, jsEvent, view) {
				
				if($('.userKalang').length) {
					
				} else {

					
					var APP_URL = {!! json_encode(url('/')) !!}

					$('#modal-event #delete').attr('data-id', calEvent._id);
					$('#kingkidama').attr("action", APP_URL + '/events/'+calEvent._id);
					$('#modal-event #_aydi').val(calEvent._id);
					$('#modal-event #_thanos').val(calEvent.status);
					$('#modal-event #_date_start').val(calEvent.start.format('YYYY-MM-DD'));
					$('#modal-event #_invisible_id').val(calEvent.start.format('HH:mm:ss'));
					$('#modal-event #potaena').val(calEvent.title);
					$('#modal-event #pangalan').val(calEvent.user.name);
					$('#modal-event #userid').val(calEvent.user_id);
                	$('#modal-event').modal('show');					
				}
            },	
			
            eventRender: function(event, element, view) {
							
              var eventEnd = moment(event.end);
			  var NOW = moment();
			
              if (eventEnd.diff(NOW, 'seconds') <= 0) {
                  return false;
              }},

              viewRender: function(currentView){
			

              var minDate = moment(),
              maxDate = moment().add(100,'weeks');
              // Past
              if (minDate >= currentView.start && minDate <= currentView.end) {
                $(".fc-prev-button").prop('disabled', true); 
                $(".fc-prev-button").addClass('fc-state-disabled'); 
              }
              else {
                $(".fc-prev-button").removeClass('fc-state-disabled'); 
                $(".fc-prev-button").prop('disabled', false); 
              }
              // Future
              if (maxDate >= currentView.start && maxDate <= currentView.end) {
                $(".fc-next-button").prop('disabled', true); 
                $(".fc-next-button").addClass('fc-state-disabled'); 
              } else {
                $(".fc-next-button").removeClass('fc-state-disabled'); 
                $(".fc-next-button").prop('disabled', false); 
			  }
			  
			 
			},

              defaultDate: strDate,
              selectHelper: true,
              selectable: true,
              defaultView: 'agendaWeek',
              minTime: '06:00:00',
              maxTime: '24:00:00',
              forceEventDuration : true,
              slotDuration: '01:00:00', 
              navLinks: false, // can click day/week names to navigate views
              editable: false,
              allDaySlot: false,
              eventOverlap: false,
              timeFormat: 'h:mm t',
              eventDurationEditable: false,
              eventLimit: false, // allow "more" link when too many events
              events: BASEURL + '/events'
            });
   
          });

		$('#delete').on('click', function() {
			  var x = $(this);
			  
			  var delete_url = x.attr('data-href')+'/'+x.attr('data-id');

			  $.ajax({
				  url: delete_url,
				  type: 'DELETE',
				  success: function(result){
					$('#modal-event').modal('hide');
					swal({
						reverseButtons: true,
						title: 'Book Successfully Deleted',
						text: "You want to delete more?",
						type: 'success',
						showCancelButton: true
					}, function() {
						window.location = "calendar";
					});
					  
					  
				  },
				  error: function(result) {
					$('#modal-event').modal('hide');
					  alert(result.message);
				  }
			  });
		  });

		  $('#deleteThis').on('click', function() {
			  var x = $(this);
			  
			  var delete_url = x.attr('data-href')+'/'+x.attr('data-id');

			  $.ajax({
				  url: delete_url,
				  type: 'DELETE',
				  success: function(result){
					$('#modal-event').modal('hide');
					swal({
						reverseButtons: true,
						title: 'Book Successfully Deleted',
						text: "You want to delete more?",
						type: 'success',
						showCancelButton: true
					}, function() {
						window.location.href = "/";
					});
					  
					  
				  },
				  error: function(result) {
					$('#modal-event').modal('hide');
					  alert(result.message);
				  }
			  });
		  });

		$('#modal-edit').on('show.bs.modal', function(event) {
			var APP_URL = {!! json_encode(url('/')) !!}

			var button = $(event.relatedTarget)
			var id = button.data('id')
			var startdate = button.data('startdate')
			var time = button.data('mytime')
			var pending = button.data('mypending')
			var title = button.data('mytitle')
			var name = button.data('name')
			var userid = button.data('userid')

			var date = moment(startdate); 
			date.format("YYYY-MM-DD"); 

			var shit = date.format("YYYY-MM-DD"); 
			var shitTime = date.format('HH:mm:ss');
			
			var modal = $(this)
			
		
			modal.find('.modal-body #_aydi').val(id);
			modal.find('.modal-body #_date_start').val(shit);
			modal.find('.modal-body #_invisible_id').val(shitTime);
			modal.find('.modal-body #_thanos').val(pending);
			modal.find('.modal-body #potaena').val(title);
			modal.find('.modal-body #pangalan').val(name);
			modal.find('.modal-body #userid').val(userid);
			modal.find('.modal-footer #deleteThis').attr('data-id', id);
			
			$('#ediwow').attr("action", APP_URL + '/events/'+id);
		
		});

		$('#modal-user-edit').on('show.bs.modal', function(event) {
			var APP_URL = {!! json_encode(url('/')) !!}
			var button = $(event.relatedTarget)
			var id = button.data('id')
			var name = button.data('name')
			var email = button.data('email')
			var role = button.data('role')
			var status = button.data('status')
			
			var modal = $(this)
				
			modal.find('.modal-body #user_id').val(id);
			modal.find('.modal-body #userName').val(name);
			modal.find('.modal-body #userEmail').val(email);
			modal.find('.modal-body #userRole').val(role);
			modal.find('.modal-body #userStatus').val(status);
			modal.find('.modal-footer #deleteUser').attr('data-id', id);
			

			$('#ediwowUser').attr("action", APP_URL + '/user/'+id);
					
		});
		  
</script>
	
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="{{ URL::asset('scripts/jquery.min.js') }}"></script>
	<script src="{{ URL::asset('scripts/modernizr.min.js') }}"></script>
	<script src="{{ URL::asset('plugin/bootstrap/js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('plugin/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js') }}"></script>
	<script src="{{ URL::asset('plugin/nprogress/nprogress.js') }}"></script>
	<script src="{{ URL::asset('plugin/sweet-alert/sweetalert.min.js') }}"></script>
	<script src="{{ URL::asset('plugin/waves/waves.min.js') }}"></script>
	<!-- Sparkline Chart -->
	<script src="{{ URL::asset('plugin/chart/sparkline/jquery.sparkline.min.js') }}"></script>
	<script src="{{ URL::asset('scripts/chart.sparkline.init.min.js') }}"></script>

	<!-- Percent Circle -->
	<script src="{{ URL::asset('plugin/percircle/js/percircle.js') }}"></script>

	<!-- Chartist Chart -->
	<script src="{{ URL::asset('plugin/chart/chartist/chartist.min.js') }}"></script>
	<script src="{{ URL::asset('scripts/jquery.chartist.init.min.js') }}"></script>

	<!-- FullCalendar -->
	<script src="{{ URL::asset('plugin/moment/moment.js') }}"></script>
	<script src="{{ URL::asset('plugin/fullcalendar/fullcalendar.min.js') }}"></script>
	<script src="{{ URL::asset('scripts/fullcalendar.init.js') }}"></script>
	
	<!-- sweet alert -->
	<script src="{{ URL::asset('scripts/sweetalert.init.min.js') }}"></script>

	<script src="{{ URL::asset('scripts/main.min.js') }}"></script>
	
	@if($message = Session::get('success'))		
		<script>
			swal({
				reverseButtons: true,
				title: 'Book Successfully added',
				text: "You want to add more?",
				type: 'success',
				showCancelButton: true
			}, function() {
				//window.location = "calendar";
				window.location.href 
			});
		</script>
	@endif

	@if($message = Session::get('update'))	
	<script>
		swal({
				reverseButtons: true,
				title: 'Record Updated',
				text: "You want to update more?",
				type: 'success',
				showCancelButton: true
			}, function() {
				//window.location = "calendar";
				window.location.href 
			});
	</script>
	@endif

</html>
